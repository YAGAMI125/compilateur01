void ForStatement(void){
	unsigned long long tag=tagNumber++;
	string var;// ou on doit stocker la valeur de la variable
	if(current!=KEYWORD||strcmp(lexer->YYText(), "FOR")!=0)
		Error("mot cle For attendu");
	current=(TOKEN) lexer->yylex(); 
	var=lexer->YYText(); 
	cout<<"For"<<tag<<":"<<endl;
	AssignementStatement();
	if(current==KEYWORD && strcmp(lexer->YYText(), "TO") ==0)
	{
		cout<<"TO"<<tag<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		Expression();
		
		cout<<"\tpush "<<variable <<endl;
		cout<<"\tpop %rbx"<<endl; 
		cout<<"\tpop %rax"<<endl;
		cout<<"\tcmpq  %rbx,%rax"<<endl;
		cout<<"\tjae EndFOR"<<tag<<"\t# if FALSE, jump out of the loop"<<tag<<endl;	//on sort de la boucle for quand la condition est véréfier
		if(current==KEYWORD||strcmp(lexer->YYText(), "DO")==0)
		{
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\tmovq" << var<< "%rbx" << endl;
			cout << "\taddq $1, %rbx" << endl;//incrementation 
			cout << "\tmovq %rbx," << var << endl;
			cout<< "\tpush %rbx"<<endl;
			cout << "\tjmp TO" << tag <<  endl; 
		}
		
	}
	if(current==KEYWORD && strcmp(lexer->YYText(), "DOWNTO")==0)
	{ 
		cout<<"DOWNTO"<<tag<<":"<<endl;  
		current=(TOKEN) lexer->yylex(); 
		Expression();
		cout<<"\tpush "<<variable <<endl;
		cout<<"\tpop %rbx"<<endl;
		cout<<"\tpop %rax"<<endl;
		cout<<"\tcmpq  %rbx,%rax"<<endl;
		cout<<"\tjbe EndFOR"<<tag<<"\t# if FALSE, jump out of the loop"<<tag<<endl;//on sort de la boucle for quand la condition est véréfier
		if(current==KEYWORD||strcmp(lexer->YYText(), "DO")==0)
		{
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\tmovq " << var << "%rbx" << endl;
			cout << "\tsubq $1, %rax" << endl;//decrementation
			cout << "\tmovq %rbx" << var << endl;
			cout<< "\tpush %rbx"<<endl;
			cout << "\tjmp DOWNTO" << tag <<  endl; 
		}

	}else{cout<<"\tmot cle to ou downto attendu"<<endl;
	cout<<"EndFOR"<<tag<<":"<<endl;
} 

}